module.exports = {
    run: async (toolbox) => {
        const { 
            print, 
        } = toolbox

        print.warning(
`Install a package
Usage:
    - vbc-audit install <pkg>+      : install package(s)
    - vbc-audit install all         : install all packages supported

Package supported:
    - slither
    - smartcheck
    - mythril
    - edchinda
`)
    },
}