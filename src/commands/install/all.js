let {tools} = require("../../tools/toolsSupported")

module.exports = {
    run: async (toolbox) => {
        for (let i = 0; i < tools.length; i++) {
            require("../../extensions/install/" + tools[i]).run(toolbox)
        }
    },
}