let {getPosition} = require("../tools/position")

module.exports = {
    name: 'smartcheck',
    dashed: true,
    alias: ["sc"],
    description: "Smartcheck audit tool",
    run: async (toolbox) => {
        const { 
            print, 
            system,
            parameters
        } = toolbox

        if (parameters.argv[3] === undefined){
            return require("./smartcheck/help").run(toolbox)
        }

        command = "smartcheck -p " + parameters.argv[3]

        print.highlight("Running command: " + command)
        
        const spinner = print.spin("waiting for smartcheck.... \n")

        system.exec(command)
        .then((res) => {
            let message = res.replace(res.substring(0, getPosition(res, "xml", 1) + 3), )

            if (message === "undefined"){
                spinner.stop()
                return
            } 

            message = message.replaceAll("SOLIDITY_", "")

            let messageArray = message.split('\n\n').filter(x => x.length);

            let faultTable = []
            let index = 0

            faultTable.push(['Id', 'Rule', 'Severity', 'Line', 'Col', 'Contents'])

            for (let i = 0; i < messageArray.length - 1; i++) {
                faultTable.push([
                    ++index,
                    messageArray[i].substring(getPosition(messageArray[i], ":", 1) + 2, getPosition(messageArray[i], "\n", 1)),
                    messageArray[i].substring(getPosition(messageArray[i], ":", 3) + 2, getPosition(messageArray[i], "\n", 3)),
                    messageArray[i].substring(getPosition(messageArray[i], ":", 4) + 2, getPosition(messageArray[i], "\n", 4)),
                    messageArray[i].substring(getPosition(messageArray[i], ":", 5) + 2, getPosition(messageArray[i], "\n", 5)),
                    (messageArray[i].substring(getPosition(messageArray[i], ":", 6) + 2, getPosition(messageArray[i], "\n", 6)).length > 200) ? "" : messageArray[i].substring(getPosition(messageArray[i], ":", 6) + 2, getPosition(messageArray[i], "\n", 6))
                ])
            }

            spinner.stop()

            print.table(
                faultTable,
                { format: 'markdown' },
            )

            print.divider()
            print.warning("/************/ TOTAL /************/")
            print.divider()

            print.info(messageArray[messageArray.length - 1])

            print.newline()
        })
        .catch((err) => {
            spinner.stop()
            print.info(err)
        })
    },
}
