module.exports = {
    run: async (toolbox) => {
        const { 
            print, 
        } = toolbox

        print.warning(
`Usage: 
    vbc-audit myth [-h] [-v LOG_LEVEL]

Positional arguments:
    safe-functions      Check functions which are completely safe using
                        symbolic execution
    analyze (default)   Triggers the analysis of the smart contract
    disassemble (d)     Disassembles the smart contract
    list-detectors      Lists available detection modules
    read-storage        Retrieves storage slots from a given address through
                        rpc
    function-to-hash    Returns the hash signature of the function
    hash-to-address     converts the hashes in the blockchain to ethereum
                        address
    version             Outputs the version

Optional arguments:
    help                show this help message and exit
    -v LOG_LEVEL         log level (0-5)
`)
    },
}