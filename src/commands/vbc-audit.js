
module.exports = {
  hidden: true,
  description: "The VBC Audit CLI",
  run: async (toolbox) => {
    return require("./help").run(toolbox)
  },
}
