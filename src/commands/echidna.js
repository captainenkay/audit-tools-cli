module.exports = {
    name: 'echidna',
    dashed: true,
    alias: ["e"],
    description: "Echidna fuzzing tool",
    run: async (toolbox) => {
        const { 
            print, 
            system,
            parameters
        } = toolbox

        if (parameters.argv[3] === undefined){
            return require("./echidna/help").run(toolbox)
        }

        command = "echidna-test"

        for (let i = 3; i < parameters.argv.length; i++) {
            command += " " + parameters.argv[i];
        }

        print.highlight("Running command: " + command)
        
        const spinner = print.spin("waiting echidna fuzzing.... \n")

        system.exec(command)
        .then((res) => {
            spinner.stop()
            print.info(res)
        })
        .catch(err => {
            let message = err.message.toString();

            message = message.replace(message.substring(0, message.indexOf("\n") + 1), "")
            
            spinner.stop()
            print.info(message);
        })
    },
}
