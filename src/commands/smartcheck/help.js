module.exports = {
    run: async (toolbox) => {
        const { 
            print
        } = toolbox

        print.warning(
`Usage: vbc-audit smartcheck target

target can be:
	- file.sol      a Solidity file
`)
    },
}