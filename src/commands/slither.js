let {getPosition} = require("../tools/position")

module.exports = {
    name: 'slither',
    dashed: true,
    alias: ["s"],
    description: "Slither audit tool",
    run: async (toolbox) => {
        const { 
            print, 
            system,
            parameters
        } = toolbox

        if (parameters.argv[3] === undefined){
            return require("./slither/help").run(toolbox)
        }

        command = "slither"

        for (let i = 3; i < parameters.argv.length; i++) {
            command += " " + parameters.argv[i];
        }

        print.warning("Running command: " + command)
        
        const spinner = print.spin("waiting for slither.... \n")

        system.exec(command)
        .then((res) => {
            spinner.stop()
            print.info(res)
        })
        .catch(res => {
            let message = res.message.toString();

            message = message.replace(message.substring(0, getPosition(message, "\n", 1) + 1), '')

            let messageArray = message.split('\n').filter(x => x.length);

            let faultTable = []
            let index = 0

            faultTable.push(['Id', 'File name', 'Line', 'Function', 'Description'])

            for (let i = 0; i < messageArray.length - 1; i++) {
                if (messageArray[i][0] === "\t" || messageArray[i].includes("Reference")) continue
                if (messageArray[i].includes("Variable")){
                    faultTable.push([
                        ++index,
                        messageArray[i].substring(getPosition(messageArray[i], "(", 2) + 1, getPosition(messageArray[i], "#", 1)).replace(/^.*[\\\/]/, ''),
                        messageArray[i].substring(getPosition(messageArray[i], "#", 1) + 1, getPosition(messageArray[i], ")", 2)),
                        messageArray[i].substring(getPosition(messageArray[i], " ", 1) + 1, getPosition(messageArray[i], " ", 2)),
                        messageArray[i].substring(0, getPosition(messageArray[i], " ", 1)) + messageArray[i].substring(getPosition(messageArray[i], ")", 2) + 1, getPosition(messageArray[i], " ", 8))
                    ])
                }

                else{
                    faultTable.push([
                        ++index,
                        messageArray[i].substring(getPosition(messageArray[i], " ", 1) + 2, getPosition(messageArray[i], "#", 1)).replace(/^.*[\\\/]/, ''),
                        messageArray[i].substring(getPosition(messageArray[i], "#", 1) + 1, getPosition(messageArray[i], ")", 2)),
                        messageArray[i].substring(0, getPosition(messageArray[i], " ", 1)),
                        messageArray[i].substring(getPosition(messageArray[i], " ", 2) + 1, getPosition(messageArray[i], ":", 1)),
                    ])
                }
            }

            spinner.stop()

            print.table(
                faultTable,
                {
                    format: 'markdown' 
                },
            )
            
            print.newline();
        })
    },
}
