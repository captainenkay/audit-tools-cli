module.exports = {
    run: async (toolbox) => {
        const { 
            print
        } = toolbox

        print.warning(
`Usage: 
    vbc-audit echidna           [--version] FILES [--contract CONTRACT] [--config CONFIG] 
                                [--format FORMAT] [--corpus-dir PATH] [--test-mode ARG] 
                                [--multi-abi] [--test-limit INTEGER] 
                                [--shrink-limit INTEGER] [--seq-len INTEGER] 
                                [--contract-addr ADDRESS] [--deployer ADDRESS] 
                                [--sender ADDRESS] [--seed SEED] [--crytic-args ARGS] 
                                [--solc-args ARGS]

EVM property-based testing framework

Available options:
    help                        Show this help text
    --version                   Show version
    FILES                       Solidity files to analyze
    --contract CONTRACT         Contract to analyze
    --config CONFIG             Config file (command-line arguments override config
                                options)
    --format FORMAT             Output format. Either 'json', 'text', 'none'. All
                                these disable interactive UI
    --corpus-dir PATH           Directory to save and load corpus and coverage data.
    --test-mode ARG             Test mode to use. Either 'property', 'assertion',
                                'dapptest', 'optimization', 'overflow' or
                                'exploration'
    --multi-abi                 Use multi-abi mode of testing.
    --test-limit INTEGER        Number of sequences of transactions to generate
                                during testing. Default is 50000
    --shrink-limit INTEGER      Number of tries to attempt to shrink a failing
                                sequence of transactions. Default is 5000
    --seq-len INTEGER           Number of transactions to generate during testing.
                                Default is 100
    --contract-addr ADDRESS     Address to deploy the contract to test. Default is
                                0x00a329c0648769A73afAc7F9381E08FB43dBEA72
    --deployer ADDRESS          Address of the deployer of the contract to test.
                                Default is 0x0000000000000000000000000000000000030000
    --sender ADDRESS            Addresses to use for the transactions sent during
                                testing. Can be passed multiple times. Check the
                                documentation to see the default values.
    --seed SEED                 Run with a specific seed.
    --crytic-args ARGS          Additional arguments to use in crytic-compile for the
                                compilation of the contract to test.
    --solc-args ARGS            Additional arguments to use in solc for the
                                compilation of the contract to test.
`)
    },
}