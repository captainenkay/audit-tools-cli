module.exports = {
    name: 'myth',
    dashed: true,
    alias: ["m"],
    description: "Mythril audit tool",
    run: async (toolbox) => {
        const { 
            print, 
            system,
            parameters,
            filesystem
        } = toolbox

        if (parameters.argv[3] === undefined){
            return require("./mythril/help").run(toolbox)
        }

        command = (filesystem.isFile(parameters.argv[3])) ? "myth analyze" : "myth"

        for (let i = 3; i < parameters.argv.length; i++) {
            command += " " + parameters.argv[i];
        }

        print.highlight("Running command: " + command)

        const spinner = print.spin("waiting mythril.... \n")

        system.exec(command)
        .then((res) => {
            spinner.stop()
            print.info(res)
        })
        .catch(res => {
            spinner.stop()
            print.info(res)
        })
    },
}
