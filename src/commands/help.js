module.exports = {
    name: 'help',
    dashed: true,
    alias: ["h"],
    description: "Displays VBC Audit help",
    run: async (toolbox) => {
        const { print } = toolbox

        print.warning(
`Vietnam Blockchain Audit tool CLI
Usage:
    - vbc-audit [option]

Options:
    - help          [h]         : show this help message and exit
    - install       [i]         : install package
    - slither       [s]         : use Slither audit tool
    - smartcheck    [sc]        : use Smartcheck audit tool
    - mythril       [m]         : use Mythril audit tool
    - echidna       [e]         : use Echidne fuzzing tool
`)
    },
}
