let {tools} = require("../tools/toolsSupported")

module.exports = {
    name: 'install',
    dashed: true,
    alias: ["i"],
    description: "Install tool",
    run: async (toolbox) => {
        const { 
            print, 
            system,
            parameters
        } = toolbox

        if (parameters.argv[3] === undefined){
            return require("./install/help").run(toolbox)
        }

        for (let i = 3; i < parameters.argv.length; i++) {
            if (tools.indexOf(parameters.argv[i]) >= 0) require("../extensions/install/" + parameters.argv[i]).run(toolbox)
            else print.error("Pacakge not supported " + parameters.argv[i])
        }
    },
}
