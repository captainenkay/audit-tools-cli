const { build } = require('gluegun')

/**
 * Create the cli and kick it off
 */
async function run(argv) {
  // create a CLI runtime
  const cli = build()
    .brand('vbc-audit')
    .exclude(["semver", "http", "template"])
    .src(__dirname)
    // .defaultCommand(require("./commands/vbc-audit"))
    .create()

    return cli.run(argv)
}

module.exports = { run }
