module.exports = {
    run: async (toolbox) => {
        const { 
            print, 
            system,
        } = toolbox

        print.warning("npm install -g slither")

        system.exec("npm install -g slither")
        .then((res) => {
            print.success("Install Slither succeed")
        })
        .catch(err => {
            print.error(err)
        })
    },
}
