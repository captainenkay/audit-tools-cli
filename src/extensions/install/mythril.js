module.exports = {
    run: async (toolbox) => {
        const { 
            print, 
            system,
        } = toolbox

        print.warning("pip3 install mythril")

        system.exec("pip3 install mythril")
        .then((res) => {
            print.success("Install Mythril succeed")
        })
        .catch(err => {
            print.error(err)
        })
    },
}
