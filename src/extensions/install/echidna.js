module.exports = {
    run: async (toolbox) => {
        const { 
            print, 
            system,
        } = toolbox

        print.warning("brew install echidna")

        system.exec("brew install echidna")
        .then((res) => {
            print.success("Install Echidna succeed")
        })
        .catch(err => {
            print.error(err)
        })
    },
}
