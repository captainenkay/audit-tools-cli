module.exports = {
    run: async (toolbox) => {
        const { 
            print, 
            system,
        } = toolbox

        print.warning("npm install -g @smartdec/smartcheck")

        system.exec("npm install -g @smartdec/smartcheck")
        .then((res) => {
            print.success("Install Smartcheck succeed")
        })
        .catch(err => {
            print.error(err)
        })
    },
}
