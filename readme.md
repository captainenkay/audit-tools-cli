# vbc-audit CLI

A CLI for vbc-audit.

## Publishing to NPM

To package your CLI up for NPM, do this:

```
$ chmod +x init.sh   
$ ./init.sh
$ vbc-audit help
```

